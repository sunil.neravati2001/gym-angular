import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TraineeProfile, TrainerProfile } from '../models/Userdata';
import { Trainer } from '../models/Trainer';

@Injectable({
  providedIn: 'root',
})
export class UserDataService {
  constructor(private http: HttpClient) {}

  getTraineeData(username: string): Observable<TraineeProfile> {
    const baseUrl = 'api/gym/trainees/profile';
    const url = `${baseUrl}/${username}`;
    return this.http.get<TraineeProfile>(url);
  }
  getTrainerData(username: string): Observable<TrainerProfile> {
    const baseUrl = 'api/gym/trainers/profile';
    const url = `${baseUrl}/${username}`;
    return this.http.get<TrainerProfile>(url);
  }

  fetchNotAssignedTrainers(username: string): Observable<Trainer[]> {
    const notAssignedTrainersUrl = `api/gym/trainees/${username}/not-assigned-trainers`;
    return this.http.get<Trainer[]>(notAssignedTrainersUrl);
  }

  deleteTraineeProfile(username: string) {
    const url = `api/gym/trainees/delete/${username}`;
    return this.http.delete(url);
  }
}
