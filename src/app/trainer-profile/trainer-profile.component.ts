import { PopupService } from './../Services/popup.service';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import { UserDataService } from '../Services/user-data.service';
import { Subject, fromEvent, takeUntil } from 'rxjs';
import { Trainee } from '../models/UpdateTrainer';
import { TrainerProfile } from '../models/Userdata';

@Component({
  selector: 'app-trainer-profile',
  templateUrl: './trainer-profile.component.html',
  styleUrls: ['./trainer-profile.component.scss'],
})
export class TrainerProfileComponent implements OnInit {
  showRegistrationLoginLinks: boolean = false;
  trainerForm!: FormGroup;
  username: string;
  userData!: TrainerProfile;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private userDataService: UserDataService,
    private popupService: PopupService
  ) {
    this.username = this.route.snapshot.params['username'];
  }
  private unsubscriber: Subject<void> = new Subject<void>();

  ngOnInit(): void {
    history.pushState(null, '');
    fromEvent(window, 'popstate')
      .pipe(takeUntil(this.unsubscriber))
      .subscribe((_) => {
        history.pushState(null, '');
      });
    this.initializeForm();
    this.fetchUserData();
  }
  ngOnDestroy(): void {
    this.unsubscriber.next();
    this.unsubscriber.complete();
  }

  initializeForm() {
    this.trainerForm = this.fb.group({
      firstName: [''],
      lastName: [''],
      specialization: [''],
      username: [''],
      isActive: [''],
      trainees: this.fb.array([]),
    });
  }

  fetchUserData() {
    this.userDataService.getTrainerData(this.username).subscribe({
      next: (response) => {
        this.userData = response;
        this.populateTrainerForm();
      },
      error: (error) => {
        this.popupService.openSuccessSnackbar('Failed to fetch user data:');
      },
    });
  }

  populateTrainerForm() {
    this.trainerForm.patchValue({
      firstName: this.userData.firstName,
      lastName: this.userData.lastName,
      specialization: this.userData.specialization,
      isActive: this.userData.isActive,
    });

    const traineesFormArray = this.trainerForm.get('trainees') as FormArray;
    traineesFormArray.clear();

    this.userData.trainees.forEach(
      (trainee:Trainee) => {
        const traineeGroup = this.fb.group({
          firstName: [trainee.firstName, [Validators.required]],
          lastName: [trainee.lastName, [Validators.required]],
        });
        traineesFormArray.push(traineeGroup);
      }
    );
  }

  get traineeControls() {
    return (this.trainerForm.get('trainees') as FormArray).controls;
  }
}
