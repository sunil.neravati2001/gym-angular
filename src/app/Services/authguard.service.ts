import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { LoginService } from './login.service';
import { jwtDecode } from 'jwt-decode';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService {
  constructor(private router: Router, private login: LoginService) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const token = localStorage.getItem('token');
    console.log('Route:', route);

    if (token) {
      const decodedToken: { sub: string } = jwtDecode(token);
      const userRole: string | null = this.login.getUserRole();
      let flag: boolean = false;

      console.log(route.params['username']);
      console.log(route.url[0].path);
      console.log(userRole);

      console.log(decodedToken);

      const requestedUsername = route.params['username'];

      if (decodedToken.sub === requestedUsername || !requestedUsername) {
        flag = true;
      } else {
        this.router.navigate(['/unauthorised-access']);
        flag = false;
      }

      const path = route.url[0].path;

      if (userRole !== null) {
        if (
          (userRole === 'trainee' && path === 'trainer-profile') ||
          (userRole === 'trainer' && path === 'trainee-profile')
        ) {
          this.router.navigate(['/unauthorised-access']);
          flag = false;
        }
      } else {
        this.router.navigate(['/unauthorised-access']);
        flag = false;
      }

      return flag;
    }

    this.router.navigate(['/unauthorised-access']);
    return false;
  }
}
