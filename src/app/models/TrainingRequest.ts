export interface TrainingRequest {
    traineeUsername: string;
    trainerUsername: string;
    trainingName: string;
    trainingDate: Date;
    trainingType: string;
    duration: number;
  }