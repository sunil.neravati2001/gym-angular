import { RegistrationResponse, TraineeRequestBody, TrainerRequestBody } from '../models/Registration';
import { User } from '../models/User';
import { PopupService } from './../Services/popup.service';
import { RegistrationHttpService } from './../Services/registration-http.service';
import { RegistrationService } from './../Services/registration.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.scss'],
})
export class RegistrationFormComponent implements OnInit {
  showRegistrationLoginLinks: boolean = false;

  registrationForm!: FormGroup;
  isTrainee: boolean = true;
  username: string = '';
  password: string = '';
  registrationSuccess: boolean = false;
  role: string = '';

  specializationOptions: string[] = [
    'Stretching',
    'Fitness',
    'Resistance',
    'Zumba',
    'Yoga',
  ];

  ngOnInit(): void {
    this.registrationForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required]],
      specialization: [null],
      dob: [''],
      address: [''],
    });

    this.registrationService.isTrainee$.subscribe((isTrainee) => {
      this.isTrainee = isTrainee;
    });
  }

  constructor(
    private fb: FormBuilder,
    private registrationService: RegistrationService,
    private registrationHttpService: RegistrationHttpService,
    private popupService: PopupService
  ) {}

  toggleRole(event: Event) {
    if (event && event.target) {
      const value = (event.target as HTMLSelectElement).value;
      this.registrationService.updateIsTrainee(value === 'true');

      if (this.isTrainee) {
        this.registrationForm.get('specialization')?.clearValidators();
        this.registrationForm.get('dob')?.setValidators([Validators.required]);
        this.registrationForm
          .get('address')
          ?.setValidators([Validators.required]);
      } else {
        this.registrationForm
          .get('specialization')
          ?.setValidators([Validators.required]);
        this.registrationForm.get('dob')?.clearValidators();
        this.registrationForm.get('address')?.clearValidators();
      }

      this.registrationForm.get('specialization')?.updateValueAndValidity();
      this.registrationForm.get('dob')?.updateValueAndValidity();
      this.registrationForm.get('address')?.updateValueAndValidity();
    }
  }

  resetForm() {
    this.registrationForm.reset();
    this.registrationSuccess = false;
    this.username = '';
    this.password = '';
  }

  onSubmit() {
    this.registrationSuccess = true;

    const userData: User = {
      firstName: this.registrationForm.value.firstName,
      lastName: this.registrationForm.value.lastName,
      email: this.registrationForm.value.email,
    };

    if (this.registrationForm.value.address) {
      userData.address = this.registrationForm.value.address;
    }

    if (this.registrationForm.value.dob) {
      userData.dateOfBirth = this.registrationForm.value.dob;
    }

    if (!this.isTrainee) {
      userData.specialization = this.registrationForm.value.specialization;
    }

    const registrationMethod = this.isTrainee
      ? this.registrationHttpService.registerTrainee(userData as TraineeRequestBody)
      : this.registrationHttpService.registerTrainer(userData as TrainerRequestBody);

    registrationMethod.subscribe({
      next: (response: RegistrationResponse) => {
        this.username = response.username;
        this.password = response.password;
      },
      error: (error) => {
        this.popupService.openSuccessSnackbar(
          `An error occurred during ${
            this.isTrainee ? 'trainee' : 'trainer'
          } registration:`
        );
      },
    });
  }
}
