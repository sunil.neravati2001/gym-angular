import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { AddTrainingRoutingModule } from './add-training-routing.module';
import { AddTrainingComponent } from './add-training.component';


@NgModule({
  declarations: [AddTrainingComponent],
  imports: [
    CommonModule,
    AddTrainingRoutingModule,
    ReactiveFormsModule
  ]
})
export class AddTrainingModule { }
