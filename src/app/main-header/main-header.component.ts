import { LoginService } from './../Services/login.service';

import { ChangeDetectorRef, Component } from '@angular/core';
import { RegistrationService } from '../Services/registration.service';
import { BehaviorSubject, Subscription, filter } from 'rxjs';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.scss'],
})
export class MainHeaderComponent {
  hideElement: boolean = true;
  isMenuShown: boolean = false;
  private routerSubscription: Subscription;
  isLoggedIn$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(
    private registrationService: RegistrationService,
    private loginService: LoginService,
    private router: Router
  ) {
    this.routerSubscription = this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe(() => {
        this.isLoggedIn$.next(!!localStorage.getItem('token'));
      });
  }

  toggleRole(isTrainee: boolean) {
    this.registrationService.updateIsTrainee(isTrainee);
  }
  toggleMenu() {
    this.isMenuShown = !this.isMenuShown;
  }

  logout() {
    this.loginService.logout();
  }

  isLoggedIn: boolean = false;

  ngOnDestroy() {
    this.routerSubscription.unsubscribe();
  }
}
