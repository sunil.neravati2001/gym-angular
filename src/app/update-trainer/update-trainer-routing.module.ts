import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UpdateTrainerComponent } from './update-trainer.component';

const routes: Routes = [
  {path:'',component:UpdateTrainerComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpdateTrainerRoutingModule { }
