import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { ActivatedRoute, Route, Router } from '@angular/router';

import { DatePipe } from '@angular/common';
import { TrainingService } from '../Services/trainingsservice.service';
import { TrainingRecord } from '../models/Training';

@Component({
  selector: 'app-trainigs-trainer',
  templateUrl: './trainigstrainer.component.html',
  styleUrls: ['./trainigstrainer.component.scss'],
})
export class TrainigsTrainerComponent implements OnInit {
  trainingsForm!: FormGroup;
  trainingsList: TrainingRecord[] = [];
  submitted: boolean = false;
  username!: string;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private trainingService: TrainingService,
    private datePipe: DatePipe
  ) {}

  ngOnInit(): void {
    this.username = this.route.snapshot.params['username'];

    this.trainingsForm = this.fb.group({
      trainerName: [''],
      specialization: [''],
      dateFrom: [''],
      dateTo: [''],
      trainingsList: this.fb.array([]),
    });

    this.fetchTrainings();
  }

  get trainingsListControls() {
    return (this.trainingsForm.get('trainingsList') as FormArray).controls;
  }

  fetchTrainings() {
    const trainerName = this.trainingsForm.get('trainerName')?.value;
    const specialization = this.trainingsForm.get('specialization')?.value;
    const dateFrom = this.trainingsForm.get('dateFrom')?.value;
    const dateTo = this.trainingsForm.get('dateTo')?.value;

    this.trainingService
      .getTraineeTrainings(
        this.username,
        trainerName,
        specialization,
        dateFrom,
        dateTo
      )
      .subscribe((trainings: TrainingRecord[]) => {
        this.trainingsList = trainings.map((training) => ({
          ...training,
          trainingDate: training.trainingDate
            ? this.datePipe.transform(
                new Date(training.trainingDate),
                'dd/MM/yyyy'
              )
            : null,
        })) as TrainingRecord[];
        this.populateTrainings();
      });
  }
  private populateTrainings() {
    const trainingsListFormArray = this.trainingsForm.get(
      'trainingsList'
    ) as FormArray;

    this.trainingsList.forEach((training) => {
      const trainingGroup = this.fb.group({
        trainingDate: [training.trainingDate, [Validators.required]],
        trainingName: [training.trainingName, [Validators.required]],
        trainingType: [training.trainingType, [Validators.required]],
        trainerName: [training.username, [Validators.required]],
        trainingDuration: [training.trainingDuration, [Validators.required]],
        specialization: [training.trainingType, [Validators.required]],
      });
      trainingsListFormArray.push(trainingGroup);
    });

    this.submitted = true;
  }

  onSubmit() {
    this.submitted = true;
    this.clearTrainings();
    this.fetchTrainings();
  }

  clearTrainings() {
    const trainingsListFormArray = this.trainingsForm.get(
      'trainingsList'
    ) as FormArray;
    while (trainingsListFormArray.length) {
      trainingsListFormArray.removeAt(0);
    }
  }
}
