import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { TraineeProfileRoutingModule } from './trainee-profile-routing.module';
import { TraineeProfileComponent } from './trainee-profile.component';


@NgModule({
  declarations: [TraineeProfileComponent],
  imports: [
    CommonModule,
    TraineeProfileRoutingModule,
    ReactiveFormsModule
  ]
})
export class TraineeProfileModule { }
