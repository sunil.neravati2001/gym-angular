import { Component } from '@angular/core';
import { RegistrationService } from './Services/registration.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers:[RegistrationService]
})
export class AppComponent {
  title = 'gym-website';
}
