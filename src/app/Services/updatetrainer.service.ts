import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TrainerupdateProfile, TrainerupdateProfileResponse } from '../models/UpdateTrainer';
import { TraineeUpdateProfile } from '../models/UpdateTrainee';

@Injectable({
  providedIn: 'root',
})
export class UpdateTrainerService {
  private apiUrl = 'api/gym/trainers/update';

  constructor(private http: HttpClient) {}

  updateTrainerProfile(trainerData: TrainerupdateProfile): Observable<TrainerupdateProfileResponse> {
    return this.http.put<TrainerupdateProfileResponse>(this.apiUrl, trainerData);
  }
}
