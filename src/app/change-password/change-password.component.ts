import { LoginService } from './../Services/login.service';
import { PopupService } from './../Services/popup.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { ChangePasswordHttpService } from '../Services/change-password-http.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {
  changePasswordForm!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private passwordService: ChangePasswordHttpService,
    private route: ActivatedRoute,
    private router: Router,
    private popupService: PopupService,
    private loginService:LoginService
  ) {}

  matchFieldsValidator: ValidatorFn = (control: AbstractControl) => {
    const newPassword = control.get('newPassword')?.value;
    const confirmPassword = control.get('newPasswordConfirmation')?.value;

    if (newPassword !== confirmPassword) {
      control
        .get('newPasswordConfirmation')
        ?.setErrors({ passwordsNotMatch: true });
    } else {
      control.get('newPasswordConfirmation')?.setErrors(null);
    }

    return null;
  };

  ngOnInit(): void {
    this.changePasswordForm = this.fb.group(
      {
        username: [this.route.snapshot.params['username'], Validators.required],
        oldPassword: ['', Validators.required],
        newPassword: ['', Validators.required],
        newPasswordConfirmation: ['', Validators.required],
      },
      { validators: this.matchFieldsValidator }
    );
  }

  onSubmit(): void {
    if (this.changePasswordForm.valid) {
      const formValues = this.changePasswordForm.value;
      const { username, oldPassword, newPassword } = formValues;
      this.passwordService
        .updatePassword(username, oldPassword, newPassword)
        .subscribe({
          next: (status: number) => {
            if (status === 200) {
              this.popupService.openSuccessSnackbar(
                'Password changed succesfully'
              );
              this.router.navigate([`/login`]);
              this.loginService.logout();
            }
          },
          error: (error) => {
            this.popupService.openSuccessSnackbar(
              'An error occurred during the password update:'
            );
          },
        });
    }
  }
}
