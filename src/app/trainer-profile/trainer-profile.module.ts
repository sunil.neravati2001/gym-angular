import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { TrainerProfileRoutingModule } from './trainer-profile-routing.module';
import { TrainerProfileComponent } from './trainer-profile.component';



@NgModule({
  declarations: [TrainerProfileComponent],
  imports: [
    CommonModule,
    TrainerProfileRoutingModule,
    ReactiveFormsModule
  ]
})
export class TrainerProfileModule { }
