import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';

import { HomeComponent } from './home/home.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UnauthorisedaccessComponent } from './unauthorisedaccess/unauthorisedaccess.component';
import { AuthGuardService } from './Services/authguard.service';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginFormComponent },
  { path: 'trainee-registration', component: RegistrationFormComponent },
  { path: 'trainer-registration', component: RegistrationFormComponent },
  {
    path: 'view-trainings/:username',
    loadChildren: () =>
      import('./trainigstrainer/trainigstrainer.module').then(
        (m) => m.TrainigstrainerModule
      ),
    canActivate: [AuthGuardService],
  },
  {
    path: 'update-trainee/:username',
    loadChildren: () =>
      import('./update-trainee/update-trainee.module').then(
        (m) => m.UpdateTraineeModule
      ),
    canActivate: [AuthGuardService],
  },
  {
    path: 'change-password/:username',
    loadChildren: () =>
      import('./change-password/change-password.module').then(
        (m) => m.ChangePasswordModule
      ),
    canActivate: [AuthGuardService],
  },
  {
    path: 'update-trainer/:username',
    loadChildren: () =>
      import('./update-trainer/update-trainer.module').then(
        (m) => m.UpdateTrainerModule
      ),
    canActivate: [AuthGuardService],
  },
  {
    path: 'view-trainer-trainings/:username',
    loadChildren: () =>
      import('./trainings-trainee/trainings-trainee.module').then(
        (m) => m.TrainingsTraineeModule
      ),
    canActivate: [AuthGuardService],
  },
  {
    path: 'add-training/:username',
    loadChildren: () =>
      import('./add-training/add-training.module').then(
        (m) => m.AddTrainingModule
      ),
    canActivate: [AuthGuardService],
  },
  {
    path: 'trainee-profile/:username',
    loadChildren: () =>
      import('./trainee-profile/trainee-profile.module').then(
        (m) => m.TraineeProfileModule
      ),
    canActivate: [AuthGuardService],
  },
  {
    path: 'trainer-profile/:username',
    loadChildren: () =>
      import('./trainer-profile/trainer-profile.module').then(
        (m) => m.TrainerProfileModule
      ),
    canActivate: [AuthGuardService],
  },
  { path: 'unauthorised-access', component: UnauthorisedaccessComponent },
  {
    path: '**',
    component: PageNotFoundComponent,
    canActivate: [AuthGuardService],
  },
];

@NgModule({
  declarations: [],
  imports: [BrowserModule, RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
