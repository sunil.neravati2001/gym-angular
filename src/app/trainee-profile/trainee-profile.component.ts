import { LoginService } from './../Services/login.service';
import { PopupService } from './../Services/popup.service';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserDataService } from '../Services/user-data.service';
import { Subject, catchError, fromEvent, takeUntil, tap } from 'rxjs';
import { DatePipe } from '@angular/common';
import { TraineeProfile } from '../models/Userdata';
import { Trainer } from '../models/Trainer';

@Component({
  selector: 'app-trainee-profile',
  templateUrl: './trainee-profile.component.html',
  styleUrls: ['./trainee-profile.component.scss'],
})
export class TraineeProfileComponent {
  showRegistrationLoginLinks: boolean = false;
  traineeForm!: FormGroup;
  username: string;
  userData!:TraineeProfile ;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private datePipe: DatePipe,
    private userDataService: UserDataService,
    private router: Router,
    private popupService: PopupService,
    private loginService: LoginService
  ) {
    this.username = this.route.snapshot.params['username'];
  }
  private unsubscriber: Subject<void> = new Subject<void>();
  ngOnInit(): void {
    history.pushState(null, '');
    fromEvent(window, 'popstate')
      .pipe(takeUntil(this.unsubscriber))
      .subscribe((_) => {
        history.pushState(null, '');
      });
    this.initializeForm();
    this.fetchUserData();
  }

  ngOnDestroy(): void {
    this.unsubscriber.next();
    this.unsubscriber.complete();
  }

  initializeForm() {
    this.traineeForm = this.fb.group({
      firstName: [''],
      lastName: [''],
      dob: [''],
      address: [''],
      isActive: [''],
      trainers: this.fb.array([]),
    });
  }

  fetchUserData() {
    this.userDataService.getTraineeData(this.username).subscribe({
      next: (response: TraineeProfile) => {
        this.userData = response;
        this.populateTraineeForm();
      },
      error: (error) => {
        this.popupService.openSuccessSnackbar('Failed to fetch user data:');
      },
    });
  }

  populateTraineeForm() {
    const formattedDob = this.userData.dateOfBirth
      ? this.datePipe.transform(this.userData.dateOfBirth, 'yyyy-MM-dd')
      : '';

    this.traineeForm.patchValue({
      firstName: this.userData.firstName,
      lastName: this.userData.lastName,
      dob: formattedDob,
      address: this.userData.address,
      isActive: this.userData.active,
    });

    const trainersFormArray = this.traineeForm.get('trainers') as FormArray;
    trainersFormArray.clear();

    this.userData.trainers.forEach(
      (trainer: Trainer) => {
        const trainerGroup = this.fb.group({
          firstName: [trainer.firstName, [Validators.required]],
          lastName: [trainer.lastName, [Validators.required]],
          username: [trainer.username, [Validators.required]],
          specialization: [trainer.specialization, [Validators.required]],
        });
        trainersFormArray.push(trainerGroup);
      }
    );
  }

  get trainerControls() {
    return (this.traineeForm.get('trainers') as FormArray).controls;
  }
  deleteProfile() {
    if (confirm('Are you sure you want to delete your profile?')) {
      this.userDataService
        .deleteTraineeProfile(this.username)
        .pipe(
          tap(() => {
            this.popupService.openSuccessSnackbar(
              'Trainee profile deleted successfully.'
            );
            this.loginService.logout();
            this.router.navigate([`/login`]);
          }),
          catchError((error) => {
            this.popupService.openSuccessSnackbar(
              'Failed to delete trainee profile'
            );
            return [];
          })
        )
        .subscribe();
    }
  }
}
