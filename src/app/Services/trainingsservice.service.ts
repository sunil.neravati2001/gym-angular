import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, map, of } from 'rxjs';
import { TrainingRecord } from '../models/Training';

@Injectable({
  providedIn: 'root',
})
export class TrainingService {
  private trainerbaseUrl = 'api/gym/training/trainer';
  private traineebaseUrl = 'api/gym/training/trainee';
  private apiUrl = 'api';

  constructor(private http: HttpClient) {}

  getTrainerTrainings(
    username: string,
    traineeName: string,
    dateFrom: string,
    dateTo: string
  ): Observable<TrainingRecord[]> {
    let params = new HttpParams().set('username', username);
    if (traineeName) {
      params = params.set('traineeName', traineeName);
    }
    if (dateFrom) {
      params = params.set('periodFrom', dateFrom);
    }
    if (dateTo) {
      params = params.set('periodTo', dateTo);
    }

    return this.http.get<TrainingRecord[]>(`${this.trainerbaseUrl}`, {
      params: params,
    });
  }

  getTraineeTrainings(
    username: string,
    trainerName: string,
    specialization: string,
    dateFrom: string,
    dateTo: string
  ): Observable<TrainingRecord[]> {
    let params = new HttpParams().set('username', username);

    if (trainerName) {
      params = params.set('trainerName', trainerName);
    }
    if (specialization) {
      params = params.set('trainingType', specialization);
    }
    if (dateFrom) {
      params = params.set('periodFrom', dateFrom);
    }
    if (dateTo) {
      params = params.set('periodTo', dateTo);
    }

    return this.http.get<TrainingRecord[]>(`${this.traineebaseUrl}`, {
      params: params,
    });
  }

  
}
