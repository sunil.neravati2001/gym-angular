import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ChangePasswordHttpService {
  constructor(private httpService: HttpClient) {}

  private baseUrl = 'api/gym';

  updatePassword(
    username: string,
    oldPassword: string,
    newPassword: string
  ): Observable<number> {
    const url = `${this.baseUrl}/updatepassword`;

    let params = new HttpParams();
    params = params.set('userName', username);
    params = params.set('oldPassword', oldPassword);
    params = params.set('newPassword', newPassword);

    return this.httpService.get(url, { params, observe: 'response' }).pipe(
      map((response) => {
        return response.status;
      })
    );
  }
}
