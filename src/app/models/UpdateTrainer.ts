export interface TrainerupdateProfileResponse {
    firstName: string;
    lastName: string;
    username: string;
    email: string;
    isActive: boolean;
    trainees: Trainee[];
  }
   
  export interface TrainerupdateProfile {
    firstName: string;
    lastName: string;
    username: string;
    email: string;
    isActive: boolean;
  }
   
  export interface Trainee {
    firstName: string;
    lastName: string;
    username: string;
  }