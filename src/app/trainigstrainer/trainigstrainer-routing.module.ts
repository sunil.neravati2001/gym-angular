import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TrainigsTrainerComponent } from './trainigstrainer.component';

const routes: Routes = [
  {path:'',component:TrainigsTrainerComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrainigstrainerRoutingModule { }
