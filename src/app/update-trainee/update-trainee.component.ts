import { PopupService } from './../Services/popup.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { UserDataService } from '../Services/user-data.service';
import { UpdateTraineeService } from '../Services/updatetrainee.service';
import { TraineeProfile } from '../models/Userdata';
import { Trainer } from '../models/Trainer';

@Component({
  selector: 'app-update-trainee',
  templateUrl: './update-trainee.component.html',
  styleUrls: ['./update-trainee.component.scss'],
})
export class UpdateTraineeComponent implements OnInit {
  showRegistrationLoginLinks: boolean = false;
  updateTraineeForm!: FormGroup;
  username!: string;
  userData!: TraineeProfile;
  notAssignedTrainers: Trainer[] = [];

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private userDataService: UserDataService,
    private datePipe: DatePipe,
    private traineeService: UpdateTraineeService,
    private router: Router,
    private popupService: PopupService
  ) {}

  ngOnInit(): void {
    this.username = this.route.snapshot.params['username'];
    this.initializeForm();
    this.fetchNotAssignedTrainers();
  }

  initializeForm() {
    const trainerControls = this.notAssignedTrainers.map((trainer) =>
      this.fb.group({
        firstName: [trainer.firstName],
        lastName: [trainer.lastName],
        username: [trainer.username],
        specialization: [trainer.specialization],
        selected: false,
      })
    );

    this.updateTraineeForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      username: [''],
      email: ['', [Validators.required, Validators.email]],
      dateOfBirth: ['', Validators.required],
      address: ['', Validators.required],
      isActive: [''],
      trainers: this.fb.array(trainerControls),
    });
  }

  fetchUserData() {
    this.userDataService.getTraineeData(this.username).subscribe({
      next: (response: TraineeProfile) => {
        this.userData = response;
        this.populateTraineeForm();
      },
      error: (error) => {
        this.popupService.openSuccessSnackbar('Failed to fetch user data');
      },
    });
  }

  fetchNotAssignedTrainers() {
    this.userDataService.fetchNotAssignedTrainers(this.username).subscribe({
      next: (response: Trainer[]) => {
        this.notAssignedTrainers = response;
        this.initializeForm();
        this.fetchUserData();
      },
      error: (error) => {
        this.popupService.openSuccessSnackbar('failed to fetch user data');
      },
    });
  }

  populateTraineeForm() {
    const formattedDateOfBirth = this.formatDateForDisplay(
      this.userData.dateOfBirth
    );

    this.updateTraineeForm.patchValue({
      firstName: this.userData.firstName,
      lastName: this.userData.lastName,
      username: this.username,
      address: this.userData.address,
      dateOfBirth: formattedDateOfBirth,
    });

    if (this.userData.active) {
      this.updateTraineeForm.get('isActive')?.setValue('true');
    } else {
      this.updateTraineeForm.get('isActive')?.setValue('false');
    }
  }

  get selectedTrainersArray() {
    return this.updateTraineeForm.get('trainers') as FormArray;
  }

  formatDateForDisplay(dateOfBirth: Date | null): string | null {
    if (!dateOfBirth) {
      return null;
    }

    const formattedDate = this.datePipe.transform(
      new Date(dateOfBirth),
      'yyyy-MM-dd'
    );
    return formattedDate || null;
  }

  toggleTrainer(index: number) {
    const control = this.selectedTrainersArray.controls[index];
    control.get('selected')?.setValue(!control.get('selected')?.value);
  }

  isTrainerSelected(index: number): boolean {
    const control = this.selectedTrainersArray.controls[index];
    return control.get('selected')?.value;
  }

  onSubmit() {
    const selectedTrainers = this.selectedTrainersArray.controls
      .filter((control) => control.get('selected')?.value)
      .map((control) => control.get('username')?.value);

    if (this.userData && this.userData.trainers) {
      this.userData.trainers.forEach((trainer: Trainer) => {
        if (trainer.username) {
          selectedTrainers.push(trainer.username);
        }
      });
    }

    const updateData = {
      firstName: this.updateTraineeForm.value.firstName,
      lastName: this.updateTraineeForm.value.lastName,
      dateOfBirth: this.updateTraineeForm.value.dateOfBirth,
      address: this.updateTraineeForm.value.address,
      email: this.updateTraineeForm.value.email,
      username: this.username,
      isActive: this.updateTraineeForm.value.isActive === 'true',
    };

    this.traineeService
      .updateTraineeProfile(this.username, updateData)
      .subscribe({
        next: (response) => {
          if (selectedTrainers.length > 0) {
            const trainerNames = selectedTrainers.join(',');

            this.traineeService
              .updateTraineeTrainers(this.username, trainerNames)
              .subscribe({
                next: (trainerResponse) => {
                  this.popupService.openSuccessSnackbar(
                    'Updated trainee suceessfully'
                  );
                },
                error: (error) => {
                  this.popupService.openSuccessSnackbar(
                    'Failed to update trainers'
                  );
                },
              });
          } else {
            this.popupService.openSuccessSnackbar(
              'No trainers selected for update.'
            );
          }
        },
        error: (error) => {
          this.popupService.openSuccessSnackbar(
            'Failed to update trainee profile'
          );
        },
      });
    this.router.navigate([`/trainee-profile/${this.username}`]);
  }
}
