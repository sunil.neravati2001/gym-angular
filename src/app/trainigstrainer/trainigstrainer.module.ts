import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { TrainigstrainerRoutingModule } from './trainigstrainer-routing.module';
import { TrainigsTrainerComponent } from './trainigstrainer.component';


@NgModule({
  declarations: [TrainigsTrainerComponent],
  imports: [
    CommonModule,
    TrainigstrainerRoutingModule,
    ReactiveFormsModule
  ]
})
export class TrainigstrainerModule { }
