export interface TrainingRecord {
    trainingName: string;
    trainingDate: Date | null;
    trainingType: string;
    trainingDuration: number;
    username: string;
  }
   