export interface Trainer {
    firstName: string;
    lastName: string;
    specialization: string;
    username: string;
  }