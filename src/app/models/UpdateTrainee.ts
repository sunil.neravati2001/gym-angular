export interface TraineeUpdateProfileResponse {
    firstName: string;
    lastName: string;
    dateOfBirth: Date;
    address: string;
    email: string;
    username: string;
    isActive: boolean;
    trainers: Trainer[];
  }
   
  export interface TraineeUpdateProfile {
    firstName: string;
    lastName: string;
    dateOfBirth: Date;
    address: string;
    email: string;
    username: string;
    isActive: boolean;
  }
   
  export interface Trainer {
    firstName: string;
    lastName: string;
    username: string;
    specialization: string;
  }