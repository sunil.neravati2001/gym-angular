import { TraineeUpdateProfile, TraineeUpdateProfileResponse, Trainer } from './../models/UpdateTrainee';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UpdateTraineeService {
  private baseUrl = 'api/gym/trainees';

  constructor(private http: HttpClient) {}

  updateTraineeProfile(username: string, traineeData: TraineeUpdateProfile): Observable<TraineeUpdateProfileResponse> {
    const url = `${this.baseUrl}/update`;
    return this.http.put<TraineeUpdateProfileResponse>(url, traineeData);
  }

  updateTraineeTrainers(
    username: string,
    trainerNames: string
  ): Observable<Trainer []> {
    const url = `${this.baseUrl}/update/${username}/trainer?trainerNames=${trainerNames}`;
    return this.http.put<Trainer []>(url, null);
  }
}
