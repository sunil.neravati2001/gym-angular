export interface RegistrationResponse {
    username: string;
    password: string;
  }
   
  export interface TrainerRequestBody {
    firstName: string;
    lastName: string;
    email: string;
    specialization: string;
  }
   
  export interface TraineeRequestBody {
    firstName: string;
    lastName: string;
    dateOfBirth: Date;
    address: string;
    email: string;
  }