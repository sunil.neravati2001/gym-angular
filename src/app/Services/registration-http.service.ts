import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RegistrationResponse, TraineeRequestBody, TrainerRequestBody } from '../models/Registration';

@Injectable({
  providedIn: 'root',
})
export class RegistrationHttpService {
  constructor(private http: HttpClient) {}

  registerTrainer(userData: TrainerRequestBody): Observable<RegistrationResponse> {
    const url = 'api/gym/trainers/registration';
    return this.http.post<RegistrationResponse>(url, userData);
  }

  registerTrainee(userData: TraineeRequestBody): Observable<RegistrationResponse> {
    const url = 'api/gym/trainees/registration';
    return this.http.post<RegistrationResponse>(url, userData);
  }
}
