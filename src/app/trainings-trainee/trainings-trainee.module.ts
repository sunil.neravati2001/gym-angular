import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { TrainingsTraineeRoutingModule } from './trainings-trainee-routing.module';
import { TrainingsTraineeComponent } from './trainings-trainee.component';


@NgModule({
  declarations: [TrainingsTraineeComponent],
  imports: [
    CommonModule,
    TrainingsTraineeRoutingModule,
    ReactiveFormsModule
  ]
})
export class TrainingsTraineeModule { }
