import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { UpdateTrainerRoutingModule } from './update-trainer-routing.module';
import { UpdateTrainerComponent } from './update-trainer.component';


@NgModule({
  declarations: [UpdateTrainerComponent],
  imports: [
    CommonModule,
    UpdateTrainerRoutingModule,
    ReactiveFormsModule
  ]
})
export class UpdateTrainerModule { }
