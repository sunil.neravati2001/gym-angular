import { PopupService } from './../Services/popup.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AddTrainingService } from '../Services/add-training.service';
import { UserDataService } from '../Services/user-data.service';
import { Trainer } from '../models/Trainer';
import { TraineeProfile } from '../models/Userdata';

@Component({
  selector: 'app-add-training',
  templateUrl: './add-training.component.html',
  styleUrls: ['./add-training.component.scss'],
})
export class AddTrainingComponent implements OnInit {
  trainingForm: FormGroup;
  trainers: string[] = [];
  userData: any;
  username: string = this.route.snapshot.params['username'];
  today=new Date();

  constructor(
    private formBuilder: FormBuilder,
    private trainingService: AddTrainingService,
    private route: ActivatedRoute,
    private userDataService: UserDataService,
    private router: Router,
    private popupService: PopupService
  ) {
    this.trainingForm = this.formBuilder.group({
      trainingDate: ['', Validators.required],
      trainingName: ['', Validators.required],
      trainingType: ['', Validators.required],
      trainerName: ['', Validators.required],
      trainingDuration: ['', [Validators.required,Validators.min(1),Validators.max(200)]]
    });
  }

  ngOnInit(): void {
    this.userDataService.getTraineeData(this.username).subscribe({
      next: (response: TraineeProfile) => {
        this.userData = response.trainers;
        this.trainers = this.userData.map(
          (trainer: Trainer) => trainer.username
        );
      },
      error: (error) => {
      this.popupService.openSuccessSnackbar("failed to fetch user data");
      },
    });
  }

  onTrainerSelect() {
    const selectedTrainer = this.trainingForm.get('trainerName')?.value;
    const trainer = this.userData.find(
      (t: Trainer) => t.username === selectedTrainer
    );
    if (trainer) {
      this.trainingForm.patchValue({ trainingType: trainer.specialization });
    }
  }

  onSubmit() {
    if (this.trainingForm.valid) {
      const username = this.route.snapshot.params['username'];
      const formValues = this.trainingForm.value;
      const requestBody = {
        traineeUsername: username,
        trainerUsername: formValues.trainerName,
        trainingName: formValues.trainingName,
        trainingDate: formValues.trainingDate,
        trainingType: formValues.trainingType,
        duration: formValues.trainingDuration,
      };

      this.trainingService.addTraining(requestBody).subscribe({
        next: (response) => {
          if (response) {
            this.router.navigate([`/view-trainings/${username}`]);
          }
        },
        error: (error) => {
          this.popupService.openSuccessSnackbar(
            'An error occurred during adding training'
          );
        },
      });
    }
  }
}
