import { HttpClient } from '@angular/common/http';
import { ChangePasswordComponent } from './../change-password/change-password.component';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class RegistrationService {
  private isTraineeSubject = new BehaviorSubject<boolean>(true);

  isTrainee$ = this.isTraineeSubject.asObservable();

  constructor(private http: HttpClient) {}

  updateIsTrainee(isTrainee: boolean) {
    this.isTraineeSubject.next(isTrainee);
  }
}
