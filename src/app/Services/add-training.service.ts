import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { TrainingRequest } from '../models/TrainingRequest';

@Injectable({
  providedIn: 'root',
})
export class AddTrainingService {
  private apiUrl = 'api';

  constructor(private http: HttpClient) {}

  addTraining(data: TrainingRequest): Observable<boolean> {
    const url = `api/gym/training/add`;
    
    return this.http.post<HttpResponse<number>>(url, data).pipe(
      map((response) => {
        return response.status >= 200 && response.status < 300;
      }),
      catchError((error) => {
        return of(false);
      })
    );
  }
}
