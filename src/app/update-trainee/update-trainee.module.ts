import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { UpdateTraineeRoutingModule } from './update-trainee-routing.module';
import { UpdateTraineeComponent } from './update-trainee.component';


@NgModule({
  declarations: [UpdateTraineeComponent],
  imports: [
    CommonModule,
    UpdateTraineeRoutingModule,
    ReactiveFormsModule
  ]
})
export class UpdateTraineeModule { }
