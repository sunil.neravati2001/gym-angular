
export interface User {
    firstName: string;
    lastName: string;
    email: string;
    dateOfBirth?: Date;
    address?: string;
    specialization?: string;
  }