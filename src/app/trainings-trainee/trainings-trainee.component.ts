
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { TrainingService } from '../Services/trainingsservice.service';
import { TrainingRecord } from '../models/Training';

@Component({
  selector: 'app-trainings-trainee',
  templateUrl: './trainings-trainee.component.html',
  styleUrls: ['./trainings-trainee.component.scss'],
})
export class TrainingsTraineeComponent implements OnInit {
  trainingsForm!: FormGroup;
  trainingsList: TrainingRecord[] = [];
  submitted: boolean = false;
  username!: string;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private trainingService: TrainingService,
    private datePipe: DatePipe
  ) {}

  ngOnInit(): void {
    this.username = this.route.snapshot.params['username'];
    this.trainingsForm = this.fb.group({
      traineeName: [''],
      dateFrom: [''],
      dateTo: [''],
      trainingsList: this.fb.array([]),
    });

    this.fetchTrainings();
  }

  get trainingsListControls() {
    return (this.trainingsForm.get('trainingsList') as FormArray).controls;
  }

  fetchTrainings() {
    const traineeName = this.trainingsForm.get('traineeName')?.value;
    const dateFrom = this.trainingsForm.get('dateFrom')?.value;
    const dateTo = this.trainingsForm.get('dateTo')?.value;

    this.trainingService
      .getTrainerTrainings(this.username, traineeName, dateFrom, dateTo)
      .subscribe((trainings: TrainingRecord[]) => {
        this.trainingsList = trainings.map((training) => ({
          ...training,
          trainingDate: training.trainingDate
            ? this.datePipe.transform(
                new Date(training.trainingDate),
                'dd/MM/yyyy'
              )
            : null,
        })) as TrainingRecord[];
        this.populateTrainings();
      });
  }

  private populateTrainings() {
    const trainingsListFormArray = this.trainingsForm.get(
      'trainingsList'
    ) as FormArray;

    this.trainingsList.forEach((training) => {
      const trainingGroup = this.fb.group({
        trainingDate: [training.trainingDate, [Validators.required]],
        trainingName: [training.trainingName, [Validators.required]],
        trainingType: [training.trainingType, [Validators.required]],
        traineeName: [training.username, [Validators.required]],
        trainingDuration: [training.trainingDuration, [Validators.required]],
      });
      trainingsListFormArray.push(trainingGroup);
    });

    this.submitted = true;
  }

  onSubmit() {
    this.submitted = true;
    this.clearTrainings();
    this.fetchTrainings();
  }

  clearTrainings() {
    const trainingsListFormArray = this.trainingsForm.get(
      'trainingsList'
    ) as FormArray;
    while (trainingsListFormArray.length) {
      trainingsListFormArray.removeAt(0);
    }
  }
}
