import { MatSnackBar } from '@angular/material/snack-bar';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root',
})
export class PopupService {
  constructor(private _snackBar: MatSnackBar) {}
  openSuccessSnackbar(message: string) {
    this._snackBar.open(message, 'Close', {
      duration: 10000,
    });
  }
}
