import { Trainer } from "./Trainer";
import { Trainee } from "./UpdateTrainer";

export interface TraineeProfile {
    firstName: string;
    lastName: string;
    dateOfBirth: Date;
    address: string;
    trainers: Trainer[];
    active: boolean;
  }
   
 export  interface TrainerProfile {
    firstName: string;
    lastName: string;
    specialization: string;
    isActive: boolean;
    trainees: Trainee[];
  }
   
 