import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TrainingsTraineeComponent } from './trainings-trainee.component';

const routes: Routes = [
  {path:'',component:TrainingsTraineeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrainingsTraineeRoutingModule { }
