import { PopupService } from './../Services/popup.service';
import { UserDataService } from './../Services/user-data.service';
import { LoginService } from './../Services/login.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginHttpService } from '../Services/login-http.service';
import { Token } from '../models/token';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {
  loginForm!: FormGroup;
  isLoading: boolean = false;
  errorMessage: string = '';
  captchaResponse: string = '';
  siteKey: string = '6LeV384oAAAAAIj4DKyGvj6v7vSjd0ejqgM-nhOF';

  constructor(
    private fb: FormBuilder,
    private loginhttpService: LoginHttpService,
    private loginService: LoginService,
    private router: Router,
    private userDataService: UserDataService,
    private popupService: PopupService
  ) {}

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      captchaResponse: ['', [Validators.required]],
    });
  }

  onLoginSubmit() {
    if (this.loginForm.valid) {
      this.isLoading = true;
      const name = this.loginForm.value.username;
      const userpassword = this.loginForm.value.password;

      this.loginhttpService.login(name, userpassword).subscribe({
        next: (response: Token) => {
          localStorage.setItem('token', response.token);
          this.userDataService.getTrainerData(name).subscribe({
            next: (response) => {
              console.log(response);
              if (response) {
                this.loginService.setUserRole('trainer');
                this.router.navigate([`/trainer-profile/${name}`]);
              }
            },
            error: (error) => {
              this.loginService.setUserRole('trainee');
              this.router.navigate([`/trainee-profile/${name}`]);
            },
          });
        },
        error: (error) => {
          this.isLoading = false;
          this.popupService.openSuccessSnackbar(
            'An error occurred while logging in.Please check all the credentials'
          );
        },
      });
    } else {
      this.isLoading = false;
      this.popupService.openSuccessSnackbar(
        'Form is not valid all details properly'
      );
    }
  }

  logout() {
    this.loginService.logout();
  }

  handleExpire() {
    this.captchaResponse = '';
  }

  handleLoad() {
    console.log('reCAPTCHA loaded');
  }
}
