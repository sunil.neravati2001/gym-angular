import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TraineeProfileComponent } from './trainee-profile.component';

const routes: Routes = [
  {path:'',component:TraineeProfileComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TraineeProfileRoutingModule { }
