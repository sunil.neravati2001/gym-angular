import { PopupService } from './../Services/popup.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserDataService } from '../Services/user-data.service';
import { UpdateTrainerService } from '../Services/updatetrainer.service';
import { TrainerProfile } from '../models/Userdata';

@Component({
  selector: 'app-update-trainer',
  templateUrl: './update-trainer.component.html',
  styleUrls: ['./update-trainer.component.scss'],
})
export class UpdateTrainerComponent implements OnInit {
  updateTrainerForm!: FormGroup;
  username!: string;
  userData!: TrainerProfile;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private userDataService: UserDataService,
    private trainerService: UpdateTrainerService,
    private popupService: PopupService
  ) {}

  ngOnInit(): void {
    this.username = this.route.snapshot.params['username'];
    this.initializeForm();
    this.fetchUserData();
  }

  initializeForm() {
    this.updateTrainerForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      username: [''],
      isActive: [''],
    });
  }

  fetchUserData() {
    this.userDataService.getTrainerData(this.username).subscribe({
      next: (response) => {
        this.userData = response;
        this.populateTrainerForm();
      },
      error: (error) => {
        this.popupService.openSuccessSnackbar('Failed to fetch user data');
      },
    });
  }

  populateTrainerForm() {
    this.updateTrainerForm.patchValue({
      firstName: this.userData.firstName,
      lastName: this.userData.lastName,
      username: this.username,
    });

    if (this.userData.isActive) {
      this.updateTrainerForm.get('isActive')?.setValue('true');
    } else {
      this.updateTrainerForm.get('isActive')?.setValue('false');
    }
  }

  onSubmit() {
    if (this.updateTrainerForm.valid) {
      const formData = this.updateTrainerForm.value;
      formData.isActive = formData.isActive === 'true';
      this.trainerService.updateTrainerProfile(formData).subscribe({
        next: (response) => {
          this.popupService.openSuccessSnackbar('Updated trainer successfully');
        },
        error: (error) => {
          this.popupService.openSuccessSnackbar('Failed to update trainer');
        },
      });
    }
    this.router.navigate([`/trainer-profile/${this.username}`]);
  }
}
